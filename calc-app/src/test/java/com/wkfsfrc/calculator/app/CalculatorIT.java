package com.wkfsfrc.calculator.app;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty"},
        glue = "com.wkfsfrc.calculator.app",
        features = "target/test-classes/features/calculation.feature",
        strict = true,
        snippets = SnippetType.CAMELCASE
)
public class CalculatorIT {

}
