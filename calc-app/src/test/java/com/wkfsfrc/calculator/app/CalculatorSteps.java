package com.wkfsfrc.calculator.app;

import com.wkfsfrc.calculator.app.Calculator;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class CalculatorSteps {

    @When("^I calculate the result from a given file named \"([^\"]*)\" to the output file named \"([^\"]*)\"$")
    public void calculateTheResultsFromFileInput(final String inputFileName, final String outputFileName) {
        Calculator.main(new String[]{
                inputFileName,
                outputFileName
        });

        assertTrue(Files.exists(Paths.get(outputFileName)));
    }

    @Then("^The calculated results from into \"([^\"]*)\" must be identical with the results from \"([^\"]*)\"$")
    public void compareOutputAndOutputExpected(final String outputFileName, final String outputExpectedFileName) throws IOException {
        try (final BufferedReader outputBufferReader = Files.newBufferedReader(Paths.get(outputFileName));
             final BufferedReader expectedBufferReader = Files.newBufferedReader(Paths.get(outputExpectedFileName))
        ) {
            String expectedLine;
            String outputLine;

            while ((expectedLine = expectedBufferReader.readLine()) != null &
                    (outputLine = outputBufferReader.readLine()) != null) {
                assertThat(outputLine, is(expectedLine));
            }
        }
    }
}
