package com.wkfsfrc.calculator.engine;

import com.wkfsfrc.calculator.engine.operation.binary.Adder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CalcEngineTest {

    @Spy
    private Adder adderFixture;
    private CalcEngine calcEngine;

    @Before
    public void setUp() {
        when(adderFixture.getOperands()).thenReturn(new Double[] {1.0, 2.0});
        when(adderFixture.getResult()).thenReturn(3.0);

        calcEngine = new CalcEngine(Collections.singletonList(adderFixture));
    }

    @Test(expected = WrongStatementException.class)
    public void calculate_add1And2And3_throwsWrongStatementException() throws WrongStatementException {
        calcEngine.calculate("addd 1.0 2.0 3.0");
    }

    @Test(expected = WrongStatementException.class)
    public void calculate_addd1And2_throwsWrongStatementException() throws WrongStatementException {
        calcEngine.calculate("addd 1.0 2.0");
    }

    @Test(expected = WrongStatementException.class)
    public void calculate_addXand2_throwsWrongStatementException() throws WrongStatementException {
        calcEngine.calculate("add X 2.0");
    }

    @Test
    public void calculate_add1And2_correctResult() throws WrongStatementException {
        String result = calcEngine.calculate("add 1.0 2.0");

        assertThat(result, is(equalTo("1.0 + 2.0 = 3.0")));

        verify(adderFixture, atLeast(1)).getOperands();
        verify(adderFixture, atLeast(1)).getResult();
    }
}