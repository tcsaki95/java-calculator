Feature: TextFileBasedCalculationFeature

  Scenario: Result output file is identical with the expected
    When I calculate the result from a given file named "${project.build.testOutputDirectory}\\input.txt" to the output file named "${project.build.testOutputDirectory}\\output.txt"
    Then The calculated results from into "${project.build.testOutputDirectory}\\output.txt" must be identical with the results from "${project.build.testOutputDirectory}\\output_expected.txt"