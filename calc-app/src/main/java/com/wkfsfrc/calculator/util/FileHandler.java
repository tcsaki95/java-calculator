package com.wkfsfrc.calculator.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileHandler {

    private BufferedReader bufferedReader;
    private BufferedWriter bufferedWriter;

    public FileHandler(String inputFileName, String outputFileName) {
        try {
            bufferedReader = new BufferedReader(new FileReader(inputFileName));
            bufferedWriter = new BufferedWriter(new FileWriter(outputFileName));
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String readFromFile() {
        try {
           return bufferedReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void writeToFile(String expression) {
        try {
            bufferedWriter.write(expression);
            bufferedWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            bufferedReader.close();
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
