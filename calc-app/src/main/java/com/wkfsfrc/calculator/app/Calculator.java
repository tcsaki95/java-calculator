package com.wkfsfrc.calculator.app;

import com.wkfsfrc.calculator.engine.CalcEngine;
import com.wkfsfrc.calculator.engine.WrongStatementException;
import com.wkfsfrc.calculator.util.FileHandler;

import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * This class is where the magic happens. The operations come from the input file as strings and they are parsed and
 * executed by CalcEngine. After the execution the results will be written into the output.txt file.
 */
public class Calculator {
    private static final int NR_OF_ARGS = 2;

    public static void main(String[] args) {

        // Validating the arguments
        try {
            validateArgs(args);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return;
        }

        // Creating a calculation engine
        CalcEngine calcEngine = new CalcEngine();

        // Configuring the calculation engine
        calcEngine.configure();


        // Creating a file handler
        FileHandler fileHandler = new FileHandler(args[0], args[1]);

        // Reading, calculating the expressions and writing them into the output file
        String expression;
        String result;
        while ((expression = fileHandler.readFromFile()) != null) {
            try {
                result = calcEngine.calculate(expression);

                fileHandler.writeToFile(result);
            } catch (WrongStatementException e) {
                System.out.println(e.getMessage());
            }
        }

        // Closing the input and output files
        fileHandler.close();
    }

    private static void validateArgs(String[] args) throws IllegalArgumentException {
        if (args.length != NR_OF_ARGS) {
            throw new IllegalArgumentException("The number of parameters must be " + NR_OF_ARGS);
        }

        if (!Files.exists(Paths.get(args[0]))) {
            throw new IllegalArgumentException("The input file doesn't exist");
        }

        if (!Files.isReadable(Paths.get(args[0]))) {
            throw new IllegalArgumentException("The input file must be readable");
        }
    }
}
