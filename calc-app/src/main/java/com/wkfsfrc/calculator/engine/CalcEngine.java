package com.wkfsfrc.calculator.engine;

import com.wkfsfrc.calculator.engine.operation.CalculationException;
import com.wkfsfrc.calculator.engine.operation.MathOperation;
import com.wkfsfrc.calculator.engine.operation.base.AggregationOperationBase;
import com.wkfsfrc.calculator.engine.operation.base.BinaryOperationBase;
import com.wkfsfrc.calculator.engine.operation.base.UnaryOperationBase;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * This class is responsible for the calculations.
 */
public class CalcEngine {
    private List<MathOperation> possibleMathOperations;

    public CalcEngine() {

    }

    CalcEngine(List<MathOperation> possibleMathOperations) {
        this.possibleMathOperations = possibleMathOperations;
    }

    /**
     * @param statement in a string format
     * @return the result as a String
     * @throws WrongStatementException if something went wrong (bad statement structure, unknown operation name, etc.)
     */
    public String calculate(String statement) throws WrongStatementException {

        String[] statementParts = statement.split(MathOperation.SEPARATOR);

        String operator = statementParts[0];

        Double[] operands = new Double[statementParts.length - 1];

        try {
            Arrays.stream(Arrays.copyOfRange(statementParts, 1, statementParts.length))
                    .map(Double::valueOf)
                    .collect(Collectors.toList())
                    .toArray(operands);
        } catch (NumberFormatException e) {
            throw new WrongStatementException("Wrong operand(s)", statement, e);
        }

        final MathOperation actualMathOperation = getMathOperation(operator);

        if (actualMathOperation == null) {
            throw new WrongStatementException("Wrong operator", statement);
        }

        try {
            actualMathOperation.calculate(operands);
        } catch (CalculationException e) {
            throw new WrongStatementException("Illegal operation", statement, e);
        }

        return actualMathOperation.toString();
    }

    public OperationType getOperationType(String operation) {
        String[] statementParts = operation.split(MathOperation.SEPARATOR);

        String operator = statementParts[0];

        final MathOperation mathOperation = getMathOperation(operator);

        OperationType actualOperationType = null;

        if (mathOperation == null) {
            actualOperationType = OperationType.UNKNOWN;
        } else if (mathOperation instanceof UnaryOperationBase) {
            actualOperationType = OperationType.UNARY;
        } else if (mathOperation instanceof BinaryOperationBase) {
            actualOperationType = OperationType.BINARY;
        } else if (mathOperation instanceof AggregationOperationBase) {
            actualOperationType = OperationType.AGGREGATION;
        }

        return actualOperationType;
    }

    public void configure() {
        final Properties properties = new Properties();
        final ClassLoader classLoader = getClass().getClassLoader();

        final InputStream inputStream = classLoader.getResourceAsStream("conf.properties");

        try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
            properties.load(bufferedReader);
        } catch (IOException e) {
            System.out.println("The configuration file is missing!");
        }

        final List<MathOperation> mathOperations = new ArrayList<>();
        properties.forEach((key, value) -> {
            try {
                Class<?> clazz = Class.forName((String) value);
                Constructor<?> constructor = clazz.getConstructor();
                Object object = constructor.newInstance();

                mathOperations.add((MathOperation) object);

            } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
                e.printStackTrace();
            }
        });

        possibleMathOperations = mathOperations;
    }

    private MathOperation getMathOperation(String operator) {
        MathOperation actualMathOperation = null;

        for (MathOperation mathOperation : possibleMathOperations) {
            if (mathOperation.getOperators().contains(operator)) {
                actualMathOperation = mathOperation;
                break;
            }
        }
        return actualMathOperation;
    }
}

