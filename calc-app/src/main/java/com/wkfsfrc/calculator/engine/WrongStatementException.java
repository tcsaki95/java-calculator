package com.wkfsfrc.calculator.engine;

/**
 * Exception thrown by the CalcEngine
 */
public class WrongStatementException extends Exception{
    WrongStatementException(String message, String operation, Throwable cause) {
        super(message + ": " + operation, cause);
    }

    WrongStatementException(String message, String operation) {
        super(message + ": " + operation);
    }
}
