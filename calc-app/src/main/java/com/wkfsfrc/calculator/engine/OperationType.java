package com.wkfsfrc.calculator.engine;

public enum OperationType {
    UNARY,
    BINARY,
    AGGREGATION,
    UNKNOWN
}
