------------------------------
||                          ||
|| USAGE OF THE APPLICATION ||
||                          ||
------------------------------

Requirements
------------

    * at least jre 1.8

What you have
-------------
    * calc-app-1.0-SNAPSHOT.jar (containing the calc-operation-api-1.0-SNAPSHOT.jar and calc-operation-impl-1.0-SNAPSHOT.jar in it)
    * calc-extension-1.0-SNAPSHOT.jar
    * calc-operation-api-1.0-SNAPSHOT.jar
    * calc-operation-impl-1.0-SNAPSHOT.jar
    * input.txt

Execution
---------

    You can easily run the application with opening a command promp and execution the following command:
        java -jar calc-app-1.0-SNAPSHOT.jar input.txt output.txt

    Note:
        In the log there is a line in the end which says there isn't an operator for max keyword.
        If you want to extend the application for solving this issue, please read the "EXTENDING THE APPLICATION" part.

-------------------------------
||                           ||
|| EXTENDING THE APPLICATION ||
||                           ||
-------------------------------

Extension
---------
    For extending the application you have to go into the calc-app-1.0-SNAPSHOT.jar and modify the conf.properties file.
    Please add the following row in the end:
        max=com.wkfsfrc.calculator.engine.operation.aggregation.Max

    After that you have to add an implementation named the same (the calc-extension-1.0-SNAPSHOT.jar contains this extension)

Execution
---------

    You can run the extended application executing the following command:
        java -cp calc-app-1.0-SNAPSHOT.jar;calc-extension-1.0-SNAPSHOT.jar com.wkfsfrc.calculator.app.Calculator input.txt output.txt


--------------------------------------------

If you have any questions please contact me:
Tamas Csaki
csaki.tamas@yahoo.com