------------------------------
||                          ||
|| USAGE OF THE APPLICATION ||
||                          ||
------------------------------

Requirements
------------

    * at least WildFly 12 application server
    * Postman

What do you have
----------------
    * calc-rest-1.0-SNAPSHOT

Execution
---------

    First you have to run the application server:
        * Go to the home directory of WildFly
        * Enter to the /bin directory and from a cmd prompt run standalone.bat

    After that you have to deploy the .war file to the server:
        * Enter to http://localhost:9990 and there you can deploy the app

    Run the Postman and import the requests.json to the app:
        * test the app with these requests

If you have any questions please contact me:
Tamas Csaki
csaki.tamas@yahoo.com