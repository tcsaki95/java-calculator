package com.wkfsfrc.calculator.util;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;

public class JsonReader {

    public byte[] readJsonDataAsByteArray(String fileName) throws IOException {
        final ClassLoader classLoader = this.getClass().getClassLoader();
        final InputStream resourceAsStream = classLoader.getResourceAsStream(fileName);

        return IOUtils.toByteArray(resourceAsStream);
    }
}
