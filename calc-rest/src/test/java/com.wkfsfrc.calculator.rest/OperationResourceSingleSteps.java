package com.wkfsfrc.calculator.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wkfsfrc.calculator.rest.dto.ResultDTO;
import com.wkfsfrc.calculator.util.JsonReader;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class OperationResourceSingleSteps {

    public static final String HTTP_LOCALHOST_8081_CALC_REST_1_0_SNAPSHOT = "http://localhost:8081/calc-rest/operation";

    private JsonReader jsonReader = new JsonReader();
    private ResultDTO resultDTO;

    @When("^I give a POST request to the /operation REST endpoint$")
    public void iGiveAPOSTRequestToTheOperationRESTEndpoint() throws IOException {
        final Client client = ClientBuilder.newClient();

        final byte[] inputJsonDataBytes = jsonReader.readJsonDataAsByteArray("single_input.json");

        resultDTO = client
                .target(HTTP_LOCALHOST_8081_CALC_REST_1_0_SNAPSHOT)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(inputJsonDataBytes, MediaType.APPLICATION_JSON_TYPE), ResultDTO.class);
    }

    @Then("^I get back a response$")
    public void iGetBackAResponse() {
        assertThat(resultDTO, notNullValue());
    }

    @And("^The response is equal to the expected$")
    public void theResponseIsEqualToTheExpected() throws IOException {
        final byte[] expectedResultBytes = jsonReader.readJsonDataAsByteArray("single_output.json");

        final ResultDTO expectedResultDTO = new ObjectMapper().readValue(expectedResultBytes, ResultDTO.class);

        assertThat(expectedResultDTO, is(resultDTO));
    }
}
