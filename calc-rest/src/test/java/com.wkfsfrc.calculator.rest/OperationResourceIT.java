package com.wkfsfrc.calculator.rest;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty"},
        glue = "com.wkfsfrc.calculator.rest",
        features = {
                "target/test-classes/features/single_rest_call.feature",
                "target/test-classes/features/batch_rest_call.feature"
        },
        strict = true,
        snippets = SnippetType.CAMELCASE
)
public class OperationResourceIT {
}
