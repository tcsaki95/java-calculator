Feature: SingleRestCallFeature

  Scenario: Response given by REST endpoint is identical to expected (single operation)
    When I give a POST request to the /operation REST endpoint
    Then I get back a response
    And The response is equal to the expected