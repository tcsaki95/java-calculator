Feature: BatchRestCallFeature

  Scenario: Response given by REST endpoint is identical to expected (batch operation)
    When I give a POST request to the /operations REST endpoint
    Then I get back a responses
    And The responses are equal to the expected