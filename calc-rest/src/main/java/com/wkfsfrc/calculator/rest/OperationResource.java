package com.wkfsfrc.calculator.rest;

import com.wkfsfrc.calculator.backend.CalculationService;
import com.wkfsfrc.calculator.backend.dto.CalcResultDTO;
import com.wkfsfrc.calculator.rest.dto.OperationDTO;
import com.wkfsfrc.calculator.rest.dto.ResultDTO;
import com.wkfsfrc.calculator.rest.mapper.ResultMapper;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Stateless
@Path("/")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public class OperationResource {

    @EJB
    private CalculationService calculationService;

    @EJB
    private ResultMapper resultMapper;

    @POST
    @Path("operation")
    public Response calculateSingleResultError(OperationDTO operationDTO) {
        final Optional<CalcResultDTO> result = calculationService.evaluateSingleOperation(operationDTO.toString());

        if (result.isPresent()) {
            final ResultDTO resultDTO = resultMapper.createOperationDTO(result.get());

            return Response.ok(resultDTO).build();
        } else {
            throw new RestApiException("wrong statement: " + operationDTO.toString());
        }
    }

    @POST
    @Path("operations")
    public Response calculateBatchResults(List<OperationDTO> operationDTOList) {
        List<ResultDTO> resultDTOList = new ArrayList<>(operationDTOList.size());

        for(OperationDTO operationDTO: operationDTOList) {
            final Optional<CalcResultDTO> result = calculationService.evaluateSingleOperation(operationDTO.toString());

            if (result.isPresent()) {
                final ResultDTO resultDTO = resultMapper.createOperationDTO(result.get());

                resultDTOList.add(resultDTO);
            }
        }

        return Response.ok()
                .entity(resultDTOList)
                .build();
    }
}
