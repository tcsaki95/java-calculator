package com.wkfsfrc.calculator.rest.mapper;

import com.wkfsfrc.calculator.backend.dto.CalcResultDTO;
import com.wkfsfrc.calculator.engine.OperationType;
import com.wkfsfrc.calculator.rest.dto.ResultDTO;

import javax.ejb.Stateless;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
public class ResultMapper {

    public ResultDTO createOperationDTO(CalcResultDTO calcResultDTO) {
        final String[] split = calcResultDTO.getCalcResultString().split(" = ");

        // Constructing the first part of the expression
        final String firstPart = split[0];

        if (firstPart.matches(".*\\(.*\\).*")) {
            final String[] firstPartSplit = firstPart.split("\\(");
            final String keyword = firstPartSplit[0];

            if (calcResultDTO.getOperationType() == OperationType.UNARY) {
                final ResultDTO<Double> resultDTO = new ResultDTO<>();

                final String operandString = firstPartSplit[1].substring(0, firstPartSplit[1].length() - 1);
                final Double operand = Double.valueOf(operandString);

                // Setting up the operand
                resultDTO.setOperands(operand);

                // Setting up the keyword
                resultDTO.setOperation(keyword);

                // Setting the result
                final Double result = Double.parseDouble(split[1]);
                resultDTO.setResult(result);

                return resultDTO;
            } else {
                final ResultDTO<List<Double>> resultDTO = new ResultDTO<>();

                final String[] operandStrings = firstPartSplit[1].substring(0, firstPartSplit[1].length() - 1)
                        .split(", ");


                final List<Double> operands = Arrays.stream(operandStrings)
                        .map(Double::valueOf)
                        .collect(Collectors.toList());

                // Setting up the operands
                resultDTO.setOperands(operands);

                // Setting up the keyword
                resultDTO.setOperation(keyword);

                // Setting the result
                final Double result = Double.parseDouble(split[1]);
                resultDTO.setResult(result);

                return resultDTO;
            }
        } else {
            final String operation = firstPart.split(" ")[1];

            final ResultDTO<List<Double>> resultDTO = new ResultDTO<>();

            // Setting up the operation
            resultDTO.setOperation(operation);

            final String[] operandStrings = firstPart.split(" . ");

            final List<Double> operands = Arrays.stream(operandStrings)
                    .map(Double::valueOf)
                    .collect(Collectors.toList());

            // Setting up the operands
            resultDTO.setOperands(operands);

            // Setting the result
            final Double result = Double.parseDouble(split[1]);
            resultDTO.setResult(result);

            return resultDTO;
        }
    }
}
