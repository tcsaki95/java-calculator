package com.wkfsfrc.calculator.rest.dto;

public class RestMessageDTO {
    private String message;

    public RestMessageDTO(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
