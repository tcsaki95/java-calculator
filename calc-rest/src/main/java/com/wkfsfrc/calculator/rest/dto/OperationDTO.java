package com.wkfsfrc.calculator.rest.dto;

import java.util.List;
import java.util.StringJoiner;

public class OperationDTO<OPERANDS_TYPE> {
    private String operation;
    private OPERANDS_TYPE operands;

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public OPERANDS_TYPE getOperands() {
        return operands;
    }

    public void setOperands(OPERANDS_TYPE operands) {
        this.operands = operands;
    }

    @Override
    public String toString() {
        StringJoiner stringJoiner = new StringJoiner(" ", operation + " ", "");

        if (operands == null) {
            return operation;
        }

        if (operands instanceof List) {
            ((List<?>)operands).stream()
                    .map(Object::toString)
                    .forEach(stringJoiner::add);
        } else {
            stringJoiner.add(operands.toString());
        }

        return stringJoiner.toString();
    }
}
