package com.wkfsfrc.calculator.rest;

import com.wkfsfrc.calculator.rest.dto.RestMessageDTO;

import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Provider
public class RestApiExceptionMapper implements ExceptionMapper<RestApiException> {

    @Override
    public Response toResponse(RestApiException exception) {
        return Response
                .serverError()
                .entity(new RestMessageDTO(exception.getMessage()))
                .build();
    }
}
