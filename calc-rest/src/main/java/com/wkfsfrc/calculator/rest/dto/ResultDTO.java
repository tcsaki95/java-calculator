package com.wkfsfrc.calculator.rest.dto;

import java.util.Objects;

public class ResultDTO<OPERANDS_TYPE> {

    private String operation;
    private OPERANDS_TYPE operands;
    private Double result;

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public OPERANDS_TYPE getOperands() {
        return operands;
    }

    public void setOperands(OPERANDS_TYPE operands) {
        this.operands = operands;
    }

    public Double getResult() {
        return result;
    }

    public void setResult(Double result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        if (!(obj instanceof ResultDTO)) {
            return false;
        } else {
            ResultDTO anotherResultDTO = (ResultDTO) obj;

            return Objects.equals(getOperation(), anotherResultDTO.getOperation()) &&
                    Objects.equals(getOperands(), anotherResultDTO.getOperands()) &&
                    Objects.equals(getResult(), anotherResultDTO.getResult());
        }
    }
}
