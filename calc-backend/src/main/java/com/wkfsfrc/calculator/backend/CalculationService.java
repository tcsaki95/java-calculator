package com.wkfsfrc.calculator.backend;

import com.wkfsfrc.calculator.backend.dto.CalcResultDTO;
import com.wkfsfrc.calculator.engine.OperationType;

import javax.ejb.Local;
import java.util.Optional;

@Local
public interface CalculationService {

    Optional<CalcResultDTO> evaluateSingleOperation(String operation);
}
