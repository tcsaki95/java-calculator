package com.wkfsfrc.calculator.backend.dto;

import com.wkfsfrc.calculator.engine.OperationType;

public class CalcResultDTO {

    private String calcResultString;
    private OperationType operationType;

    public CalcResultDTO() {
    }

    public CalcResultDTO(String calcResultString, OperationType operationType) {
        this.calcResultString = calcResultString;
        this.operationType = operationType;
    }

    public String getCalcResultString() {
        return calcResultString;
    }

    public void setCalcResultString(String calcResultString) {
        this.calcResultString = calcResultString;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }
}
