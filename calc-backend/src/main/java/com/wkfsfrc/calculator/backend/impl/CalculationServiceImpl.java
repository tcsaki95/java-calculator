package com.wkfsfrc.calculator.backend.impl;

import com.wkfsfrc.calculator.backend.CalculationService;
import com.wkfsfrc.calculator.backend.dto.CalcResultDTO;
import com.wkfsfrc.calculator.engine.CalcEngine;
import com.wkfsfrc.calculator.engine.OperationType;
import com.wkfsfrc.calculator.engine.WrongStatementException;

import javax.ejb.Stateless;
import java.util.Optional;

@Stateless
public class CalculationServiceImpl implements CalculationService {

    private CalcEngine calcEngine;

    public CalculationServiceImpl() {
        calcEngine = new CalcEngine();

        calcEngine.configure();
    }

    @Override
    public Optional<CalcResultDTO> evaluateSingleOperation(String operation) {
        try {
            final String resultString = calcEngine.calculate(operation);
            final OperationType resultOperationType = calcEngine.getOperationType(operation);

            return Optional.of(new CalcResultDTO(resultString, resultOperationType));
        } catch (WrongStatementException e) {
            System.out.println("Wrong statement: " + operation);
            return Optional.empty();
        }
    }
}
