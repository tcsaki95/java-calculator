package com.wkfsfrc.calculator.engine.operation.aggregation;

import com.wkfsfrc.calculator.engine.operation.base.AggregationOperationBase;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Sum extends AggregationOperationBase {

    @Override
    public List<String> getOperators() {
        return Collections.singletonList("sum");
    }

    @Override
    protected void executeOnlyAggregationCalculation() {
        setResult(Arrays.stream(getOperands()).mapToDouble(Double::doubleValue).sum());
    }
}
