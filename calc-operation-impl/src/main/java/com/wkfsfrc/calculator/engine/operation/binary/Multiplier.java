package com.wkfsfrc.calculator.engine.operation.binary;

import com.wkfsfrc.calculator.engine.operation.base.BinaryOperationBase;

import java.util.Arrays;
import java.util.List;

public class Multiplier extends BinaryOperationBase {

    @Override
    public List<String> getOperators() {
        return Arrays.asList("*", "multiply");
    }

    @Override
    protected void executeOnlyBinaryCalculation() {
        setResult(getOperands()[0] * getOperands()[1]);
    }
}
