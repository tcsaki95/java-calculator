package com.wkfsfrc.calculator.engine.operation.binary;

import com.wkfsfrc.calculator.engine.operation.base.BinaryOperationBase;

import java.util.Arrays;
import java.util.List;

public class Pow extends BinaryOperationBase {

    @Override
    public List<String> getOperators() {
        return Arrays.asList("^", "pow");
    }

    @Override
    protected void executeOnlyBinaryCalculation() {
        setResult(Math.pow(getOperands()[0], getOperands()[1]));
    }
}
