package com.wkfsfrc.calculator.engine.operation.aggregation;

import com.wkfsfrc.calculator.engine.operation.base.AggregationOperationBase;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Avg extends AggregationOperationBase {

    @Override
    public List<String> getOperators() {
        return Collections.singletonList("avg");
    }

    @Override
    protected void executeOnlyAggregationCalculation() {
        setResult(Arrays.stream(getOperands()).mapToDouble(Double::doubleValue).sum() / getOperands().length);
    }
}
