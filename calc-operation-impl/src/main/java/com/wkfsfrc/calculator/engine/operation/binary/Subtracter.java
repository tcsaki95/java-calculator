package com.wkfsfrc.calculator.engine.operation.binary;

import com.wkfsfrc.calculator.engine.operation.base.BinaryOperationBase;

import java.util.Arrays;
import java.util.List;

public class Subtracter extends BinaryOperationBase {

    @Override
    public List<String> getOperators() {
        return Arrays.asList("-", "subtract");
    }

    @Override
    protected void executeOnlyBinaryCalculation() {
        setResult(getOperands()[0] - getOperands()[1]);
    }
}
