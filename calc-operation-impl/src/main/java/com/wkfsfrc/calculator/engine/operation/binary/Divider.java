package com.wkfsfrc.calculator.engine.operation.binary;

import com.wkfsfrc.calculator.engine.operation.CalculationException;
import com.wkfsfrc.calculator.engine.operation.base.BinaryOperationBase;

import java.util.Arrays;
import java.util.List;

public class Divider extends BinaryOperationBase {

    @Override
    public List<String> getOperators() {
        return Arrays.asList("/", "divide");
    }

    @Override
    protected void executeOnlyBinaryCalculation() throws CalculationException {
        if (getOperands()[1] == 0.0) {
            throw new CalculationException("Division by zero");
        }

        setResult(getOperands()[0] / getOperands()[1]);
    }
}
