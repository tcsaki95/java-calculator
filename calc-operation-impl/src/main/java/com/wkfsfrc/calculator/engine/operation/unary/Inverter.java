package com.wkfsfrc.calculator.engine.operation.unary;

import com.wkfsfrc.calculator.engine.operation.CalculationException;
import com.wkfsfrc.calculator.engine.operation.base.UnaryOperationBase;

import java.util.Collections;
import java.util.List;

public class Inverter extends UnaryOperationBase {

    @Override
    public List<String> getOperators() {
        return Collections.singletonList("inv");
    }

    @Override
    protected void executeOnlyUnaryCalculation() throws CalculationException {
        if (getOperands()[0] == 0.0) {
            throw new CalculationException("Division by zero");
        }

        setResult(1 / getOperands()[0]);
    }
}
