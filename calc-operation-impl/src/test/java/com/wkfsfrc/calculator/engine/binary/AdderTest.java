package com.wkfsfrc.calculator.engine.binary;

import com.wkfsfrc.calculator.engine.operation.binary.Adder;
import com.wkfsfrc.calculator.engine.operation.CalculationException;
import org.junit.Test;


import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class AdderTest {

    @Test
    public void calculate_add2And3_getResultReturnsTo5() throws CalculationException {
        Adder adder = new Adder();

        adder.calculate(2.0, 3.0);

        assertThat(adder.getResult(), is(5.0));
    }

}