package com.wkfsfrc.calculator.engine.binary;

import com.wkfsfrc.calculator.engine.operation.CalculationException;
import com.wkfsfrc.calculator.engine.operation.binary.Subtracter;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class SubtracterTest {

    @Test
    public void calculate_subtract3And2_getResultReturnsTo1() throws CalculationException {
        Subtracter subtracter = new Subtracter();

        subtracter.calculate(3.0, 2.0);

        assertThat(subtracter.getResult(), is(equalTo(1.0)));
    }

}