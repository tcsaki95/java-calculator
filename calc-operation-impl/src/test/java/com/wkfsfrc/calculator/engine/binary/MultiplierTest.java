package com.wkfsfrc.calculator.engine.binary;

import com.wkfsfrc.calculator.engine.operation.CalculationException;
import com.wkfsfrc.calculator.engine.operation.binary.Multiplier;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class MultiplierTest {

    @Test
    public void calculate_multiply3And2_getResultReturnsTo2() throws CalculationException {
        Multiplier subtracter = new Multiplier();

        subtracter.calculate(3.0, 2.0);

        assertThat(subtracter.getResult(), is(equalTo(6.0)));
    }
}