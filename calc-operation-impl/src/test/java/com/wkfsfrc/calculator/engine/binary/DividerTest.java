package com.wkfsfrc.calculator.engine.binary;

import com.wkfsfrc.calculator.engine.operation.CalculationException;
import com.wkfsfrc.calculator.engine.operation.binary.Divider;
import org.junit.Before;
import org.junit.Test;


import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class DividerTest {

    private Divider divider;

    @Before
    public void setUp() {
        divider = new Divider();
    }

    @Test
    public void calculate_divide1By2_getResultReturns0_5() throws CalculationException {
        divider.calculate(1.0, 2.0);

        assertThat(divider.getResult(), is(equalTo(0.5)));
    }

    @Test(expected = CalculationException.class)
    public void calculate_divide1By0_throwsCalculationException() throws CalculationException {
        divider.calculate(1.0, 0.0);
    }
}