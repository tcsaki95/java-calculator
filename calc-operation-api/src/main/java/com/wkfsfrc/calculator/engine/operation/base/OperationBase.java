package com.wkfsfrc.calculator.engine.operation.base;

import com.wkfsfrc.calculator.engine.operation.CalculationException;
import com.wkfsfrc.calculator.engine.operation.MathOperation;

/**
 * This class contains a base for implementing an operation.
 */
public abstract class OperationBase implements MathOperation {
    static final String WRONG_NUMBER_OF_OPERANDS = "Wrong number of operands";
    private Double[] operands;
    private double result;

    /**
     * Getting the operands.
     * @return the operands
     */
    public Double[] getOperands() {
        return operands;
    }

    /**
     * Setting the operands.
     * @param operands the given operands as an array
     */
    private void setOperands(Double[] operands) {
        this.operands = operands;
    }

    @Override
    public double getResult() {
        return result;
    }

    /**
     * Setting the result of the operation.
     * @param result the given value
     */
    public void setResult(double result) {
        this.result = result;
    }

    @Override
    public void calculate(Double... operands) throws CalculationException {
        setOperands(operands);

        execute();
    }

    /**
     * This method is where the actual calculation will be implemented.
     * @throws CalculationException if something goes wrong
     */
    protected abstract void execute() throws CalculationException;
}
