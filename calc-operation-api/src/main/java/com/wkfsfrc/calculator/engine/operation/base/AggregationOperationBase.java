package com.wkfsfrc.calculator.engine.operation.base;

import com.wkfsfrc.calculator.engine.operation.CalculationException;

import java.util.StringJoiner;
import java.util.stream.Stream;

/**
 * This abstract class is the base for aggregation operations.
 */
public abstract class AggregationOperationBase extends OperationBase {

    @Override
    protected void execute() throws CalculationException {
        if (getOperands().length < 1) {
            throw new CalculationException(WRONG_NUMBER_OF_OPERANDS);
        }

        executeOnlyAggregationCalculation();
    }

    @Override
    public String toString() {
        StringJoiner stringJoiner = new StringJoiner(", ", getOperators().get(0) + "(", ")");
        Stream.of(getOperands())
                .map(d -> Double.toString(d))
                .forEach(stringJoiner::add);

        return stringJoiner.toString() + " = " + String.format("%.1f", getResult());
    }

    /**
     * This method is where the actual aggregation calculation will be implemented (without the common exception handling).
     * @throws CalculationException if something goes wrong with the calculation
     */
    protected abstract void executeOnlyAggregationCalculation() throws CalculationException;
}
