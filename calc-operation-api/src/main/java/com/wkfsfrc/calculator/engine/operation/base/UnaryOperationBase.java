package com.wkfsfrc.calculator.engine.operation.base;

import com.wkfsfrc.calculator.engine.operation.CalculationException;

/**
 * This abstract class is the base for unary operations.
 */
public abstract class UnaryOperationBase extends OperationBase {

    @Override
    protected void execute() throws CalculationException {
        if (getOperands().length != 1) {
            throw new CalculationException(WRONG_NUMBER_OF_OPERANDS);
        }

        executeOnlyUnaryCalculation();
    }

    @Override
    public String toString() {
        return String.format("%s(%.1f) = %.1f", getOperators().get(0), getOperands()[0], getResult());
    }

    /**
     * This method is where the actual unary calculation will be implemented (without the common exception handling).
     * @throws CalculationException if something goes wrong with the calculation
     */
    protected abstract void executeOnlyUnaryCalculation() throws CalculationException;
}
