package com.wkfsfrc.calculator.engine.operation.base;

import com.wkfsfrc.calculator.engine.operation.CalculationException;

import java.util.StringJoiner;

/**
 * This abstract class is the base for binary operations.
 */
public abstract class BinaryOperationBase extends OperationBase {

    @Override
    protected void execute() throws CalculationException {
        if (getOperands().length != 2) {
            throw new CalculationException(WRONG_NUMBER_OF_OPERANDS);
        }

        executeOnlyBinaryCalculation();
    }

    @Override
    public String toString() {
        StringJoiner stringJoiner = new StringJoiner(" " +getOperators().get(0) + " ");
        stringJoiner.add(Double.toString(getOperands()[0]));
        stringJoiner.add(Double.toString(getOperands()[1]));

        return stringJoiner.toString() + " = " + getResult();
    }

    /**
     * This method is where the actual binary calculation will be implemented (without the common exception handling).
     * @throws CalculationException if something goes wrong with the calculation
     */
    protected abstract void executeOnlyBinaryCalculation() throws CalculationException;
}
