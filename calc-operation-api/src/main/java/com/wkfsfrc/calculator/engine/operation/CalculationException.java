package com.wkfsfrc.calculator.engine.operation;

/**
 * Exception thrown by the calculation classes.
 */
public class CalculationException extends Exception {
    public CalculationException(String message) {
        super(message);
    }
}
