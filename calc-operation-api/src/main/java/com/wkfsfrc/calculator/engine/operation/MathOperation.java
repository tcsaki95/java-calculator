package com.wkfsfrc.calculator.engine.operation;

import java.util.List;

/**
 * This interface represents a math operation.
 */
public interface MathOperation {
    String SEPARATOR = " ";

    /**
     *
     * @return the operator as a list of strings
     */
    List<String> getOperators();

    /**
     * @param operands the operands of the statement
     * @throws CalculationException when something goes wrong
     */
    void calculate(Double... operands) throws CalculationException;

    /**
     *
     * @return the calculated result
     */
    double getResult();
}
